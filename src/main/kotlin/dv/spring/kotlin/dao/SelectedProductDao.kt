package dv.spring.kotlin.dao

import dv.spring.kotlin.entity.SelectedProduct
import dv.spring.kotlin.entity.dto.DisplayProduct
import org.springframework.data.domain.Page

interface SelectedProductDao {
    fun getSelectedProducts(): List<SelectedProduct>
    fun getSelectedProductWithPage(name: String, page: Int, pageSize: Int): Page<SelectedProduct>
     fun save(selectedProduct: SelectedProduct): SelectedProduct

}