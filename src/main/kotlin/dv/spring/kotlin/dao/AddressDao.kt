package dv.spring.kotlin.dao


import dv.spring.kotlin.entity.Address
import dv.spring.kotlin.entity.dto.AddressDto

interface AddressDao{
     fun save(address: Address): Address
     fun findById(adressId: Long): Address?

}