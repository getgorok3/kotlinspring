package dv.spring.kotlin.dao

import dv.spring.kotlin.entity.Product
import org.springframework.data.domain.Page

interface ProductDao {
    fun getProducts(): List<Product>
    fun getProductByName(name: String): Product?
    fun getProductByPartialNmae(name: String): List<Product>
    fun getProductByPartialAllNmaeAndDesc(name: String, desc: String): List<Product>
    fun getProductByManuName(nsme: String): List<Product>
    fun getProductWithPage(name: String, page: Int, pageSize: Int): Page<Product>
    fun save(product: Product): Product
    fun findById(id: Long): Product?


}