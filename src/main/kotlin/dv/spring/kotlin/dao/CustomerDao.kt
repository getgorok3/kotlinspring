package dv.spring.kotlin.dao

import dv.spring.kotlin.entity.Customer
import dv.spring.kotlin.entity.UserStatus
import org.springframework.data.domain.Page

interface CustomerDao {
    fun getCustomers(): List<Customer>
    fun getCustomerByName(name: String): Customer?
    fun getCustomerByPartialName(name: String): List<Customer>
    fun getCustomerByPartialNameAndEmail(name: String, email: String): List<Customer>
    fun getCustomerByProvinceName(name: String): List<Customer>
    fun getAllCustomersByStatusName(name: String): List<Customer>
    fun save(customer: Customer): Customer
    fun findById(id: Long): Customer?


}