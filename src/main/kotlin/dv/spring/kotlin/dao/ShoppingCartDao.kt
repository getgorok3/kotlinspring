package dv.spring.kotlin.dao

import dv.spring.kotlin.entity.Customer
import dv.spring.kotlin.entity.ShoppingCart
import org.springframework.data.domain.Page

interface ShoppingCartDao {
    fun getShoppingCarts(): List<ShoppingCart>
    fun getAllShoppingCartsWithPage(page: Int, pageSize: Int): Page<ShoppingCart>
    fun getShoppingCartsWithPageByName(name: String, page: Int, pageSize: Int): Page<ShoppingCart>
    fun getCustomerByTheirProductNameWithPage(name: String): List<Customer>
    fun save(shoppingCart: ShoppingCart): ShoppingCart
    fun getShoppingCartByCustomerId(custoemrId: Long): ShoppingCart
    fun getShoppingCartByProductName(name: String): List<ShoppingCart>
}