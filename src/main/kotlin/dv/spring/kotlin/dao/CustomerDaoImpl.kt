//package dv.spring.kotlin.dao
//
//import dv.spring.kotlin.entity.Customer
//import dv.spring.kotlin.entity.UserStatus
//import org.springframework.context.annotation.Profile
//import org.springframework.stereotype.Repository
//
//@Profile("mem")
//@Repository
//class CustomerDaoImpl : CustomerDao {
//    override fun getCustomers(): List<Customer> {
//        return mutableListOf(Customer("Lung", "pm@go.th",
//                UserStatus.ACTIVE),
//                Customer("ชัชชาติ", "chut@taopoon.com",
//                        UserStatus.ACTIVE),
//                Customer("ธนาธร", "thanathorn@life.com",
//                        UserStatus.PENDING))
//    }
//}