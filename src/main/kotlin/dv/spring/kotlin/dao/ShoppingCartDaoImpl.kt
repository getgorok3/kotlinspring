//package dv.spring.kotlin.dao
//
//import dv.spring.kotlin.entity.ShoppingCart
//import dv.spring.kotlin.entity.ShoppingCartStatus
//import org.springframework.context.annotation.Profile
//import org.springframework.stereotype.Repository
//
//@Profile("mem")
//@Repository
//class ShoppingCartDaoImpl: ShoppingCartDao{
//    override fun getShoppingCarts(): List<ShoppingCart> {
//        return mutableListOf(ShoppingCart(ShoppingCartStatus.SENT),
//                ShoppingCart(ShoppingCartStatus.WAIT))
//    }
//}