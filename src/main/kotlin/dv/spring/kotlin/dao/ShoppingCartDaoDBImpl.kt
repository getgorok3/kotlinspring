package dv.spring.kotlin.dao

import dv.spring.kotlin.entity.Customer
import dv.spring.kotlin.entity.ShoppingCart
import dv.spring.kotlin.repository.CustomerRepository
import dv.spring.kotlin.repository.ShoppingCartRepository
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.context.annotation.Profile
import org.springframework.data.domain.Page
import org.springframework.data.domain.PageRequest
import org.springframework.stereotype.Repository

@Profile("db")
@Repository
class ShoppingCartDaoDBImpl: ShoppingCartDao{
    override fun getShoppingCartByProductName(name: String): List<ShoppingCart> {
        return shoppingCartRepository
                .findBySelectedProduct_Product_NameContainingIgnoreCase(name)
    }

    override fun getShoppingCartByCustomerId(custoemrId: Long): ShoppingCart {
        return shoppingCartRepository.findByCustomer_Id(custoemrId)
    }

    override fun save(shoppingCart: ShoppingCart): ShoppingCart {
        return shoppingCartRepository.save(shoppingCart)
    }

    override fun getCustomerByTheirProductNameWithPage(name: String): List<Customer> {
        var c : MutableList<Customer> = ArrayList()
        var shoppingCartByProduct = shoppingCartRepository.findBySelectedProduct_Product_NameContainingIgnoreCase(name)
        for( each in shoppingCartByProduct){
            each.customer?.name.let{
                c.add(customerRepository.findByName(each.customer?.name!!))
            }
        }
        return c
    }

    override fun getShoppingCartsWithPageByName(name: String, page: Int, pageSize: Int): Page<ShoppingCart> {
        return shoppingCartRepository.findBySelectedProduct_Product_NameContainingIgnoreCase(name,PageRequest.of(page,pageSize))
    }

    override fun getAllShoppingCartsWithPage(page: Int, pageSize: Int): Page<ShoppingCart> {
        return shoppingCartRepository.findAll(PageRequest.of(page,pageSize))
    }

    @Autowired
    lateinit var shoppingCartRepository: ShoppingCartRepository

    @Autowired
    lateinit var customerRepository: CustomerRepository

    override fun getShoppingCarts(): List<ShoppingCart> {
        return shoppingCartRepository.findAll().filterIsInstance(ShoppingCart::class.java)
    }
}