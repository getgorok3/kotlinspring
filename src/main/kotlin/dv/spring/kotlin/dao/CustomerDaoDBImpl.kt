package dv.spring.kotlin.dao

import dv.spring.kotlin.entity.Customer
import dv.spring.kotlin.entity.Product
import dv.spring.kotlin.entity.UserStatus
import dv.spring.kotlin.repository.CustomerRepository
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.context.annotation.Profile
import org.springframework.data.domain.Page
import org.springframework.stereotype.Repository
import java.lang.IllegalArgumentException


@Profile("db")
@Repository
class CustomerDaoDBImpl : CustomerDao {
    override fun findById(id: Long): Customer? {
        return customerRepository.findById(id).orElse(null)
    }

    override fun save(customer: Customer): Customer {
        return customerRepository.save(customer)
    }


    override fun  getAllCustomersByStatusName(name: String): List<Customer> {
        try{
            return customerRepository
                    .findByUserStatus(UserStatus.valueOf(name.toUpperCase()))
        }catch (e:IllegalArgumentException){
            return  emptyList()
        }
    }

    override fun getCustomerByProvinceName(name: String): List<Customer> {
        return customerRepository.findByDefaultAddress_ProvinceContainingIgnoreCase(name)
    }

    override fun getCustomerByPartialNameAndEmail(name: String, email: String): List<Customer> {
        return customerRepository.findByNameContainingIgnoreCaseOrEmailContainingIgnoreCase(name,email)
    }

    override fun getCustomerByPartialName(name: String): List<Customer> {
        return  customerRepository.findByNameContainingIgnoreCase(name)
    }

    @Autowired
    lateinit var customerRepository: CustomerRepository

    override fun getCustomers(): List<Customer> {
//        return customerRepository.findAll().filterIsInstance(Customer::class.java)
   return customerRepository.findByIsDeletedIsFalse()
    }

    override fun getCustomerByName(name: String): Customer? {
        return customerRepository.findByName(name)
    }
}