//package dv.spring.kotlin.dao
//
//import dv.spring.kotlin.entity.SelectedProduct
//import org.springframework.context.annotation.Profile
//import org.springframework.stereotype.Repository
//
//@Profile("mem")
//@Repository
//class SelectedProductDaoImpl: SelectedProductDao{
//    override fun getSelectedProducts(): List<SelectedProduct> {
//        return mutableListOf(SelectedProduct(4),
//                SelectedProduct(1),
//                SelectedProduct(1),
//                SelectedProduct(1),
//                SelectedProduct(2))
//    }
//}