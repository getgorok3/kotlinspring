package dv.spring.kotlin.dao

import dv.spring.kotlin.entity.Manufacturer
import dv.spring.kotlin.entity.Product

interface ManufacturerDao {
    fun getMenufacturers(): List<Manufacturer>
    fun save(manufacturer: Manufacturer): Manufacturer
    fun findById(id: Long): Manufacturer?
}