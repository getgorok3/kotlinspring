package dv.spring.kotlin.repository

import dv.spring.kotlin.entity.Manufacturer
import org.springframework.data.repository.CrudRepository

interface ManufactureRepository: CrudRepository<Manufacturer, Long>{
    fun findByName(name: String): Manufacturer

}