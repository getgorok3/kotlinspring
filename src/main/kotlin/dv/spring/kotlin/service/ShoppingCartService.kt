package dv.spring.kotlin.service

import dv.spring.kotlin.entity.Customer
import dv.spring.kotlin.entity.ShoppingCart
import dv.spring.kotlin.entity.dto.ShoppingCustomerDto
import org.springframework.data.domain.Page

interface ShoppingCartService {
    fun getShoppingCarts(): List<ShoppingCart>
    fun getAllShoppingCartsWithPage(page: Int, pageSize: Int): Page<ShoppingCart>
    fun getShoppingCartsWithPageByName(name: String, page: Int, pageSize: Int): Page<ShoppingCart>
//    fun getCustomerByTheirProductNameWithPage(name: String): List<Customer>
    fun save(shoppingCustomerDto: ShoppingCustomerDto): ShoppingCart
    fun save(customerID: Long, shoppingCustomerDto: ShoppingCustomerDto): ShoppingCart

}