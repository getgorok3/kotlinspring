package dv.spring.kotlin.service

import dv.spring.kotlin.dao.CustomerDao
import dv.spring.kotlin.dao.ProductDao
import dv.spring.kotlin.dao.SelectedProductDao
import dv.spring.kotlin.dao.ShoppingCartDao
import dv.spring.kotlin.entity.*
import dv.spring.kotlin.entity.dto.ShoppingCustomerDto
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.data.domain.Page
import org.springframework.stereotype.Service
import javax.transaction.Transactional

@Service
class ShoppingCartServiceImpl: ShoppingCartService{
    @Transactional
    override fun save(customerID: Long, shoppingCustomerDto: ShoppingCustomerDto): ShoppingCart {
        val customer = customerDao.findById(customerID)
        val shoppingCart = ShoppingCart(
                status = ShoppingCartStatus.WAIT
        )
        for(each in shoppingCustomerDto.products!!){
            var product = each.id?.let { productDao.findById(it) }
            var selectedProduct = SelectedProduct(each.quantity)
            selectedProduct.product = product
            selectedProductDao.save(selectedProduct)
            shoppingCart.selectedProduct.add(selectedProduct)
        }
        shoppingCart.customer = customer
        shoppingcartDao.save(shoppingCart)

        return shoppingCart

    }
    override fun save(shoppingCustomerDto: ShoppingCustomerDto): ShoppingCart {
        val shoppingCart = ShoppingCart(
                status = ShoppingCartStatus.WAIT
        )
        for(each in shoppingCustomerDto.products!!){
            var product = each.id?.let { productDao.findById(it) }
            var selectedProduct = SelectedProduct(each.quantity)
            selectedProduct.product = product
            selectedProductDao.save(selectedProduct)
            shoppingCart.selectedProduct.add(selectedProduct)
        }
        val customer = shoppingCustomerDto.customer?.let { customerDao.getCustomerByName(it) }
        shoppingCart.customer = customer
        shoppingcartDao.save(shoppingCart)

        return shoppingCart
    }

//    override fun getCustomerByTheirProductNameWithPage(name: String): List<Customer> {
//        return shoppingcartDao.getCustomerByTheirProductNameWithPage(name)
//    }

    override fun getShoppingCartsWithPageByName(name: String, page: Int, pageSize: Int): Page<ShoppingCart> {
        return shoppingcartDao.getShoppingCartsWithPageByName(name,page,pageSize)
    }

    override fun getAllShoppingCartsWithPage(page: Int, pageSize: Int): Page<ShoppingCart> {
        return shoppingcartDao.getAllShoppingCartsWithPage(page,pageSize)
    }

    @Autowired
    lateinit var shoppingcartDao: ShoppingCartDao

    @Autowired
    lateinit var selectedProductDao: SelectedProductDao

    @Autowired
    lateinit var customerDao : CustomerDao

    @Autowired
    lateinit var productDao: ProductDao

    @Transactional
    override fun getShoppingCarts(): List<ShoppingCart> {
        return shoppingcartDao.getShoppingCarts()
    }
}