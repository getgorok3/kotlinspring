package dv.spring.kotlin.service

import dv.spring.kotlin.entity.Manufacturer
import dv.spring.kotlin.entity.Product
import dv.spring.kotlin.entity.dto.ManufacturerDto

interface ManufacturerService{
    fun getMenufacturers(): List<Manufacturer>
     fun save(manu: ManufacturerDto): Manufacturer

}