package dv.spring.kotlin.service

import dv.spring.kotlin.dao.AddressDao
import dv.spring.kotlin.dao.CustomerDao
import dv.spring.kotlin.dao.ShoppingCartDao
import dv.spring.kotlin.entity.Customer
import dv.spring.kotlin.entity.UserStatus
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.data.domain.Page
import org.springframework.stereotype.Service
import javax.transaction.Transactional

@Service
class CustomerServiceImpl : CustomerService {
    @Transactional
    override fun getCustomerByTheirProductName(name: String): List<Customer> {
        return shoppingCartDao.getShoppingCartByProductName(name)
                .mapNotNull { shoppingCart -> shoppingCart.customer }
                .toSet().toList()
    }

    @Transactional
    override fun remove(id: Long): Customer? {
      val customer = custumerDao.findById(id)
        customer?.isDeleted = true
        return customer
    }

    @Transactional
    override fun save(adressId: Long, customer: Customer): Customer {
        val address = addressDao.findById(adressId)
        val customer = custumerDao.save(customer)
        customer.defaultAddress =address
        return customer
    }

    @Transactional
    override fun save(customer: Customer): Customer {
        val defaultAddress = customer.defaultAddress?.let { addressDao.save(it) }
        val billing_address = customer.billing_address?.let { addressDao.save(it) }
        val shippingAdress = customer.shippingAdress?.let {
            for (each in it) {
                addressDao.save(each)
            }
        }
        val customer = custumerDao.save(customer)
        return customer
    }

    override fun getAllCustomersByStatusName(name: String): List<Customer> {
        return custumerDao.getAllCustomersByStatusName(name)
    }

    override fun getCustomerByProvinceName(name: String): List<Customer> {
        return custumerDao.getCustomerByProvinceName(name)
    }

    override fun getCustomerByPartialNameAndEmail(name: String, email: String): List<Customer> {
        return custumerDao.getCustomerByPartialNameAndEmail(name, email)
    }

    override fun getCustomerByPartialName(name: String): List<Customer> {
        return custumerDao.getCustomerByPartialName(name)
    }

    @Autowired
    lateinit var custumerDao: CustomerDao
    @Autowired
    lateinit var addressDao: AddressDao
    @Autowired
    lateinit var shoppingCartDao: ShoppingCartDao

    @Transactional
    override fun getCustomers(): List<Customer> {
        return custumerDao.getCustomers()
    }

    override fun getCustomerByName(name: String): Customer? {
        val customer = custumerDao.getCustomerByName(name)
        if(customer?.isDeleted != true)
            return customer
        return null
//        customer?.isDeleted = false
//        return customer
    }
}