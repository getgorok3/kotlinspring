package dv.spring.kotlin.service

import dv.spring.kotlin.entity.Customer
import dv.spring.kotlin.entity.UserStatus
import org.springframework.data.domain.Page

interface CustomerService {
    fun getCustomers(): List<Customer>
    fun getCustomerByName(name: String): Customer?
    fun getCustomerByPartialName(name: String): List<Customer>
    fun getCustomerByPartialNameAndEmail(name: String, email: String): List<Customer>
    fun getCustomerByProvinceName(name: String): List<Customer>
    fun getAllCustomersByStatusName(name: String): List<Customer>
    fun save(customer: Customer): Customer
    fun save(adressId: Long, customer: Customer): Customer
    fun remove(id: Long): Customer?
    fun getCustomerByTheirProductName(name: String): List<Customer>

}