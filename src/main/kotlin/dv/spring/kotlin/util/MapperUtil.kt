package dv.spring.kotlin.util

import dv.spring.kotlin.entity.*
import dv.spring.kotlin.entity.dto.*
import dv.spring.kotlin.security.entity.Authority
import org.mapstruct.InheritInverseConfiguration
import org.mapstruct.Mapper
import org.mapstruct.Mapping
import org.mapstruct.Mappings
import org.mapstruct.factory.Mappers

@Mapper(componentModel = "spring")
interface MapperUtil {
    companion object {
        val INSTANCE = Mappers.getMapper(MapperUtil::class.java)
    }

    @Mappings(
            Mapping(source = "manufacturer", target = "manu")
    )


    fun mapProductDto(product: Product?): ProductDto?

    fun mapProductDto(products: List<Product>): List<ProductDto>

    @InheritInverseConfiguration
    fun mapProductDto(productDto: ProductDto): Product


    fun mapManufacturer(manu: Manufacturer): ManufacturerDto
    fun mapManufacturer(manu: List<Manufacturer>): List<ManufacturerDto>

//    @Mappings(
//            Mapping(source = "customer.jwtUser.username", target = "username"),
//            Mapping(source = "customer.jwtUser.authorities", target = "authorities")
//    )
    @Mappings(
            Mapping(source = "customer.jwtUser.username", target = "username"),
            Mapping(source = "customer.jwtUser.authorities", target = "authorities")
    )
    fun mapUser(customer: Customer): UserDto
    fun mapCustomerDto(customer: Customer?): CustomerDto?

    fun mapCustomerDto(customer: List<Customer>): List<CustomerDto>
    @InheritInverseConfiguration
    fun mapCustomerDto(customerDto: CustomerDto): Customer

    fun mapAuthority(authority: Authority): AuthorityDto

    fun mapAuthority(authority: List<Authority>): List<AuthorityDto>


    fun mapShoppingCartDto(shoppingCart: ShoppingCart): ShoppingCartDto
    fun mapShoppingCartDto(shoppingCart: List<ShoppingCart>): List<ShoppingCartDto>

    fun mapSelectedProductDto(selectedProduct: SelectedProduct?): SelectedProductDto?
    fun mapSelectedProductDto(selectedProduct: List<SelectedProduct>): List<SelectedProductDto>

    @InheritInverseConfiguration
    fun mapManufacturer(manu: ManufacturerDto): Manufacturer

    fun mapAddress(address: Address): AddressDto
    fun mapAddress(address: List<Address>): List<AddressDto>
    @InheritInverseConfiguration
    fun mapAddress(address: AddressDto): Address


}