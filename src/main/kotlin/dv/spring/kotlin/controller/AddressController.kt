package dv.spring.kotlin.controller

import dv.spring.kotlin.entity.dto.AddressDto
import dv.spring.kotlin.service.AddressService
import dv.spring.kotlin.util.MapperUtil
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.*

@RestController
class AddressController{
    @Autowired
    lateinit var addressService: AddressService
    @PostMapping("/address")
    fun addAddress(@RequestBody addresss: AddressDto): ResponseEntity<Any> {
        return ResponseEntity.ok(MapperUtil.INSTANCE.mapAddress(
                addressService.save(addresss)
        ))
    }

    @PutMapping("/address/{addressID}")
    fun updateAddressWithId(@PathVariable("addressID") id: Long?
                                 , @RequestBody addresss: AddressDto): ResponseEntity<Any> {
        addresss.id = id
        return ResponseEntity.ok (
            MapperUtil.INSTANCE.mapAddress(addressService.save(addresss))
        )
    }
}