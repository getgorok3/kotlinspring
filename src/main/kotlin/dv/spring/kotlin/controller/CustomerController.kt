package dv.spring.kotlin.controller

import dv.spring.kotlin.entity.UserStatus
import dv.spring.kotlin.entity.dto.CustomerDto
import dv.spring.kotlin.service.CustomerService
import dv.spring.kotlin.util.MapperUtil
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.*

@RestController
class CustomerController {

    @Autowired
    lateinit var custumerService: CustomerService

    @GetMapping("/customer")
    fun getAllCustomer(): ResponseEntity<Any> {
        val customers = custumerService.getCustomers()
        return ResponseEntity.ok(MapperUtil.INSTANCE.mapCustomerDto(customers))

    }

    @GetMapping("/customer/query")
    fun getAllCustomer(@RequestParam("name") name: String): ResponseEntity<Any> {
//        var output = MapperUtil.INSTANCE.mapCustomerDto(custumerService.getCustomerByName(name))
//        output?.let { return ResponseEntity.ok(it) }
//        return ResponseEntity.status(HttpStatus.NOT_FOUND).build()
        val customer = custumerService.getCustomerByName(name)
        val outputCustomer = MapperUtil.INSTANCE.mapCustomerDto(customer)
        outputCustomer?.let{ return ResponseEntity.ok(it)}
        return ResponseEntity.status(HttpStatus.NOT_FOUND).body("The customer is not found")
    }

    @GetMapping("/customer/partialQuery")
    fun getCustomerPartial(@RequestParam("name") name: String): ResponseEntity<Any> {
        var output = MapperUtil.INSTANCE.mapCustomerDto(custumerService.getCustomerByPartialName(name))
        return ResponseEntity.ok(output)
    }

    @GetMapping("/customer/partialQueryEmails")
    fun getgetCustomerPartial(@RequestParam("name") name: String,
                              @RequestParam("email", required = false) email: String?): ResponseEntity<Any> {
//        val output:List<Any>
//        if(email == null){
//            output = MapperUtil.INSTANCE.mapCustomerDto(custumerService.getCustomerByPartialName(name))
//        }else{
//            output = MapperUtil.INSTANCE.mapCustomerDto(custumerService.getCustomerByPartialNameAndEmail(name,email))
//        }
        val output = MapperUtil.INSTANCE.mapCustomerDto(
                custumerService.getCustomerByPartialNameAndEmail(name, name)
        )
        return ResponseEntity.ok(output)

    }

    @GetMapping("/customer/provinceName")
    fun getCustomerByProvinceName(@RequestParam("name") name: String): ResponseEntity<Any> {
        val output = MapperUtil.INSTANCE.mapCustomerDto(
                custumerService.getCustomerByProvinceName(name)
        )
        return ResponseEntity.ok(output)
    }

    @GetMapping("/customer/status")
    fun getAllCustomersByStatusName(@RequestParam("name") name: String): ResponseEntity<Any> {

        val output = MapperUtil.INSTANCE.mapCustomerDto(
                custumerService.getAllCustomersByStatusName(name)
        )
        return ResponseEntity.ok(output)
    }

    @PostMapping("/customer")
    fun addCustomer(@RequestBody customerDto: CustomerDto): ResponseEntity<Any> {
        val output = custumerService.save(MapperUtil.INSTANCE.mapCustomerDto(customerDto))
        val outputDto = MapperUtil.INSTANCE.mapCustomerDto(output)
        outputDto?.let{ return ResponseEntity.ok(it)}
        return ResponseEntity.status(HttpStatus.NOT_FOUND).build()
    }
    @PostMapping("/customer/address/{adressId}")
    fun addCustomerByAddressId(@RequestBody customerDto: CustomerDto,
                               @PathVariable adressId: Long): ResponseEntity<Any> {
        val output = custumerService.save(adressId,MapperUtil.INSTANCE.mapCustomerDto(customerDto))
        val outputDto = MapperUtil.INSTANCE.mapCustomerDto(output)
        outputDto?.let{ return ResponseEntity.ok(it)}
        return ResponseEntity.status(HttpStatus.NOT_FOUND).build()
    }

    @DeleteMapping("/customer/{id}")
    fun deleteCustomer(@PathVariable("id") id:Long): ResponseEntity<Any> {
        val customer = custumerService.remove(id)
        val outputCustomer = MapperUtil.INSTANCE.mapCustomerDto(customer)
        outputCustomer?.let{ return ResponseEntity.ok(it)}
        return ResponseEntity.status(HttpStatus.NOT_FOUND).body("The customer is not found")
    }

    @GetMapping("/customers/products")
    fun getCustomerByTheirProductName(@RequestParam("name") name: String): ResponseEntity<Any> {
        val output = MapperUtil.INSTANCE.mapCustomerDto(
                custumerService.getCustomerByTheirProductName(name)
        )
        output?.let{ return ResponseEntity.ok(it)}

    }





}