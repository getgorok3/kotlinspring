//package dv.spring.kotlin.controller
//
//import dv.spring.kotlin.entity.Product
//import org.springframework.http.ResponseEntity
//import org.springframework.web.bind.annotation.*
//
//@RestController
//class AssessmentController{
//    var testOutput = mutableListOf<Product>()
//    @GetMapping("/getAppName")
//    fun getAppName(): String {
//        return "assessment"
//    }
//
//    @GetMapping( "/product")
//    fun getProduct(): ResponseEntity<Any> {
//        val product = Product("iPhone", "A new telephone", 28000,5)
//        return ResponseEntity.ok(product)
//    }
//
//    @GetMapping( "/product/{name}")
//    fun getProductWithPath(@PathVariable("name") name: String): ResponseEntity<Any> {
//        val product = Product("iPhone", "A new telephone", 28000,5)
//        testOutput.add(product)
//        var result: ResponseEntity<Any> = ResponseEntity.ok("init" )
//        for( i in testOutput){
//            if(i.name.equals(name)){
//                result = ResponseEntity.ok(i)
//            }else{
//                result = ResponseEntity.notFound().build()
//            }
//        }
//        return result
//    }
//
////    @GetMapping( "/product/iPhoneXS")
////    fun getProductWithPathNotfound(): ResponseEntity<Any> {
////
////        return ResponseEntity.notFound().build()
////    }
//
//    @PostMapping( "/setZeroQuantity")
//    fun resetQuantity(@RequestBody product: Product ): ResponseEntity<Any>{
//        product.quantity = 0
//        return ResponseEntity.ok(product )
//    }
//
//    @PostMapping( "/totalPrice")
//    fun getTotalPrice(@RequestBody products: Array<Product> ): String{
//        var totalPrice = 0
//        for( i in products){
//            totalPrice = totalPrice + i.price
//        }
//        return "$totalPrice"
//    }
//
//    @PostMapping( "/avaliableProduct")
//    fun getAvaliableProduct(@RequestBody products: Array<Product> ): ResponseEntity<Any>{
//        var output = mutableListOf<Product>()
//        for(i in products){
//            if(i.quantity != 0) {
//                output.add(i)
//            }
//        }
//        return ResponseEntity.ok(output)
//    }
//
//
//
//
//
//}
//
//
//
