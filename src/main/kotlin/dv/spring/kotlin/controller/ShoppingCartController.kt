package dv.spring.kotlin.controller

import dv.spring.kotlin.entity.Customer
import dv.spring.kotlin.entity.dto.DisplayProduct
import dv.spring.kotlin.entity.dto.PageCustomerProductNameDto
import dv.spring.kotlin.entity.dto.PageShoppingCartDto
import dv.spring.kotlin.entity.dto.ShoppingCustomerDto
import dv.spring.kotlin.service.ShoppingCartService
import dv.spring.kotlin.util.MapperUtil
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.*

@RestController
class ShoppingCartController {
    @Autowired
    lateinit var shoppingCartService: ShoppingCartService

    @GetMapping("/shoppingcart")
    fun getShoppingCarts(): ResponseEntity<Any> {
        val shoppingCarts = shoppingCartService.getShoppingCarts()
        return ResponseEntity.ok(MapperUtil.INSTANCE.mapShoppingCartDto(shoppingCarts))
    }

    @GetMapping("/shoppingcarts")
    fun getAllShoppingCartsWithPage(@RequestParam("page") page: Int,
                                    @RequestParam("pageSize") pageSize: Int): ResponseEntity<Any> {
        val output = shoppingCartService.getAllShoppingCartsWithPage(page, pageSize)
        return ResponseEntity.ok(PageShoppingCartDto(totalPages = output.totalPages,
                totalElement = output.totalElements,
                shoppingCarts = MapperUtil.INSTANCE.mapShoppingCartDto(output.content)))
    }

    @GetMapping("/shoppingcarts/name")
    fun getShoppingCartsWithPageByName(@RequestParam("name") name: String,
                                       @RequestParam("page") page: Int,
                                       @RequestParam("pageSize") pageSize: Int): ResponseEntity<Any> {
        val output = shoppingCartService.getShoppingCartsWithPageByName(name, page, pageSize)
        return ResponseEntity.ok(PageShoppingCartDto(totalPages = output.totalPages,
                totalElement = output.totalElements,
                shoppingCarts = MapperUtil.INSTANCE.mapShoppingCartDto(output.content)))
    }
    
    @PostMapping("/shoppingcarts")
    fun addShoppingcarts(@RequestBody shoppingCustomerDto: ShoppingCustomerDto): ResponseEntity<Any> {
        val output = MapperUtil.INSTANCE.mapShoppingCartDto(
                shoppingCartService.save(shoppingCustomerDto))

        var displayProduct = mutableListOf<DisplayProduct>()
        for (each in output.selectedProduct!!) {
            displayProduct.add(DisplayProduct(id = each.product?.id,
                    description = each.product?.description, quantity = each?.quantity))
        }
        return ResponseEntity.ok(ShoppingCustomerDto(customer = output.customer?.name,
                defaultAddress = output.customer?.defaultAddress, products = displayProduct))
    }

    @PostMapping("/shoppingcarts/customer/{customerID}")
    fun addShoppingCartByCustomerIdAndProductId(
            @PathVariable customerID: Long,
            @RequestBody shoppingCustomerDto: ShoppingCustomerDto): ResponseEntity<Any> {
        val output = MapperUtil.INSTANCE.mapShoppingCartDto(
                shoppingCartService.save(customerID,shoppingCustomerDto))

        var displayProduct = mutableListOf<DisplayProduct>()
        for (each in output.selectedProduct!!) {
            displayProduct.add(DisplayProduct(name = each.product?.name,
                    id = each.product?.id,
                    description = each.product?.description, quantity = each?.quantity))
        }
        return ResponseEntity.ok(ShoppingCustomerDto(customer = output.customer?.name,
                defaultAddress = output.customer?.defaultAddress, products = displayProduct))
    }
}