package dv.spring.kotlin.controller

import dv.spring.kotlin.entity.dto.PageProductDto
import dv.spring.kotlin.entity.dto.ProductDto
import dv.spring.kotlin.service.ProductService
import dv.spring.kotlin.util.MapperUtil
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.*

@RestController
class ProductController {

    @Autowired
    lateinit var productService: ProductService

    @GetMapping("/product")
    fun getProducts(): ResponseEntity<Any> {
        val products = productService.getProducts()
        return ResponseEntity.ok(MapperUtil.INSTANCE.mapProductDto(products))

//        val productDtos = mutableListOf<ProductDto>()
//        for (product in products) {
//            productDtos.add(ProductDto(product.name,
//                    product.description,
//                    product.price,
//                    product.amountInStock,
//                    product.imageUrl))
//        }
//        return ResponseEntity.ok(productDtos)
    }

    @GetMapping("/product/query")
    fun getProducts(@RequestParam("name") name: String): ResponseEntity<Any> {
        var output = MapperUtil.INSTANCE.mapProductDto(productService.getProductByName(name))
        output?.let { return ResponseEntity.ok(it) }
        return ResponseEntity.status(HttpStatus.NOT_FOUND).build()
    }

    @GetMapping("/products/partialQuery")
    fun getProductsPartial(@RequestParam("name") name: String,
                           @RequestParam("desc", required = false) desc: String?): ResponseEntity<Any> {
        // need name no or have desc
//        val output:List<Any>
//        if(desc == null){
//            output =  MapperUtil.INSTANCE.mapProductDto(productService.getProductByPartialName(name))
//        }else{
//            output =  MapperUtil.INSTANCE.mapProductDto(productService.getProductByPartialNameAndDesc(name,desc))
//        }
//        return ResponseEntity.ok(output)

        // one keyword for all param
        val output = MapperUtil.INSTANCE.mapProductDto(
                productService.getProductByPartialAllNameAndDesc(name, name)
        )
        return ResponseEntity.ok(output)
    }

    @GetMapping("/product/{manuName}")
    fun getProductByManuName(@PathVariable("manuName") name: String): ResponseEntity<Any> {
        val output = MapperUtil.INSTANCE.mapProductDto(
                productService.getProductByManuName(name)
        )
        return ResponseEntity.ok(output)
    }

    @GetMapping("/product/name")
    fun getProductWithPage(@RequestParam("name") name: String,
                           @RequestParam("page") page: Int,
                           @RequestParam("pageSize") pageSize: Int): ResponseEntity<Any> {
        val output = productService.getProductWithPage(name, page, pageSize)
        return ResponseEntity.ok(PageProductDto(totalPages = output.totalPages,
                totalElement = output.totalElements,
                products = MapperUtil.INSTANCE.mapProductDto(output.content)))
    }

    @PostMapping("/product/name")
    fun addProduct(@RequestBody productDto: ProductDto): ResponseEntity<Any> {
        val output = productService.save(MapperUtil.INSTANCE.mapProductDto(productDto))
        val outputDto = MapperUtil.INSTANCE.mapProductDto(output)
        outputDto?.let { return ResponseEntity.ok(it) }
        return ResponseEntity.status(HttpStatus.NOT_FOUND).build()
    }

    @PostMapping("/product/manufacturer/{manuId}")
    fun addProductBymanufacturerId(@RequestBody productDto: ProductDto,
                                   @PathVariable manuId: Long): ResponseEntity<Any> {
        val output = productService.save(manuId,MapperUtil.INSTANCE.mapProductDto(productDto))

        val outputDto =MapperUtil.INSTANCE.mapProductDto(output)
        outputDto?.let { return ResponseEntity.ok(it) }
        return ResponseEntity.status(HttpStatus.NOT_FOUND).build()
    }

    @DeleteMapping("/product/{id}")
    fun deleteProduct(@PathVariable("id") id:Long): ResponseEntity<Any> {
        val product = productService.remove(id)
        val outputProduct = MapperUtil.INSTANCE.mapProductDto(product)
        outputProduct?.let{ return ResponseEntity.ok(it)}
        return ResponseEntity.status(HttpStatus.NOT_FOUND).body("The product is not found")
    }

}