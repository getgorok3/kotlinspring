package dv.spring.kotlin.entity

import javax.persistence.*

@Entity
data class ShoppingCart(var status: ShoppingCartStatus?) {
    @Id
    @GeneratedValue
    var id: Long? = null
    @OneToMany
    var selectedProduct = mutableListOf<SelectedProduct>()

    @OneToOne
    var shippingAdress: Address? = null

    @OneToOne
    var customer: Customer? = null
}