package dv.spring.kotlin.entity.dto

data class PageSelectedProductDto(var totalPages: Int? =null,
                                  var totalElement: Long? = null,
                                  var selectedProducts: List<SelectedProductDto> = mutableListOf())