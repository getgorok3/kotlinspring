package dv.spring.kotlin.entity.dto

import dv.spring.kotlin.entity.Address


data class ShoppingCustomerDto(var customer: String? = null,
                               var defaultAddress: Address? = null,
                               var products: List<DisplayProduct>? =null)