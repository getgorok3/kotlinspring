package dv.spring.kotlin.entity.dto

data class PageCustomerProductNameDto(var totalPages: Int? =null,
                                      var totalElement: Long? = null,
                                      var customers: List<CustomerDto> = mutableListOf())