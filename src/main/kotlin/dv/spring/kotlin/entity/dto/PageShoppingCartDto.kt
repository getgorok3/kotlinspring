package dv.spring.kotlin.entity.dto

data class PageShoppingCartDto(var totalPages: Int? =null,
                               var totalElement: Long? = null,
                               var shoppingCarts: List<ShoppingCartDto> = mutableListOf())