package dv.spring.kotlin.entity.dto

import dv.spring.kotlin.entity.Address
import dv.spring.kotlin.entity.Customer
import dv.spring.kotlin.entity.SelectedProduct
import dv.spring.kotlin.entity.ShoppingCartStatus

data class ShoppingCartDto(var status: ShoppingCartStatus? = ShoppingCartStatus.WAIT,
                           var selectedProduct: List<SelectedProductDto>? = mutableListOf<SelectedProductDto>(),
                           var shippingAdress: Address? = null,
                           var customer: Customer? = null)