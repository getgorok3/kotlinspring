package dv.spring.kotlin.entity

enum class UserStatus {
    PENDING, ACTIVE, NOTACTIVR, DELETED
}