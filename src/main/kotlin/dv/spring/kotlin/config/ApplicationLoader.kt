package dv.spring.kotlin.config

import dv.spring.kotlin.entity.*
import dv.spring.kotlin.repository.*
import dv.spring.kotlin.security.entity.Authority
import dv.spring.kotlin.security.entity.AuthorityName
import dv.spring.kotlin.security.entity.JwtUser
import dv.spring.kotlin.security.repository.AuthorityRepository
import dv.spring.kotlin.security.repository.UserRepository
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.ApplicationArguments
import org.springframework.boot.ApplicationRunner
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder
import org.springframework.stereotype.Component
import javax.transaction.Transactional

@Component
class ApplicationLoader : ApplicationRunner {
    @Autowired
    lateinit var manufacturerRepository: ManufacturerRepository

    @Autowired
    lateinit var authorityRepository: AuthorityRepository

    @Autowired
    lateinit var productRepository: ProductRepository

    @Autowired
    lateinit var customerRepository: CustomerRepository

    @Autowired
    lateinit var shoppingCartRepository: ShoppingCartRepository

    @Autowired
    lateinit var addressRepository: AddressRepository

    @Autowired
    lateinit var selectedProductRepository: SelectedProductRepository

    @Autowired
    lateinit var dataLoader: DataLoader

    @Autowired
    lateinit var userRepository: UserRepository

    @Transactional
    fun loadUsernameAndPassword() {
        val auth1 = Authority(name = AuthorityName.ROLE_ADMIN)
        val auth2 = Authority(name = AuthorityName.ROLE_CUSTOMER)
        val auth3 = Authority(name = AuthorityName.ROLE_GENERAL)
        authorityRepository.save(auth1)
        authorityRepository.save(auth2)
        authorityRepository.save(auth3)
        var encoder = BCryptPasswordEncoder()
        val custl = Customer(name = " สมชาติิ", email = "a@b.com")
        val custlJwt = JwtUser(
                username = "customer",
                password = encoder.encode("password"),
                email = custl.email,
                enabled = true,
                firstname = custl.name,
                lastname = "unknown"
        )

        customerRepository.save(custl)
        userRepository.save(custlJwt)
        custl.jwtUser = custlJwt
        custlJwt.user = custl
        custlJwt.authorities.add(auth2)
        custlJwt.authorities.add(auth3)

    }

    @Transactional
    override fun run(args: ApplicationArguments?) {
        var manul = manufacturerRepository.save(Manufacturer("CAMT", "0000000"))
        var manu2 = manufacturerRepository.save(Manufacturer("SAMSUNG", "555666777888"))
        var manu3 = manufacturerRepository.save(Manufacturer("Apple", "053123456"))


        var product1 = productRepository.save(Product("CAMT", "The best Collage in CMU", 0.0,
                1, "http://www.camt.cmu.ac.th/th/images/logo.jpg"))
        var product2 = productRepository.save(Product("iPhone", "It's a phone", 28000.0, 20,
                Manufacturer("Apple", "053123456"), "https://www.jaymartstore.com/Products/iPhone-X-64GB-Space-Grey--1140900010552--4724"))
        var product3 = productRepository.save(Product("Prayuth", "The best PM ever", 1.0, 1,
                Manufacturer("CAMT", "0000000"), "https://upload.wikimedia.org/wikipedia/commons/thumb/9/9a/Prayut_Chan-o-cha_%28cropped%29_2016.jpg/200px-Prayut_Chan-o-cha_%28cropped%29_2016.jpg"
        ))
        var product4 = productRepository.save(Product("Note 9", "Other Iphone", 28001.0, 10,
                Manufacturer("Samsung", "555666777888"), "http://dynamic-cdn.eggdigital.com/e56zBiUt1.jpg"
        ))
        var customer1 = customerRepository.save(Customer("Lung", "pm@go.th",
                UserStatus.ACTIVE))
        var address1 = addressRepository.save(Address("ถนนอนุสาวรีย์ประชาธิปไตย", "แขวง ดินสอ",
                "เขตดุสิต", "กรุงเทพ", "10123"))

        var customer2 = customerRepository.save(Customer("ชัชชาติ", "chut@taopoon.com",
                UserStatus.ACTIVE))
        var address2 = addressRepository.save(Address("239 มหาวิทยาลัยเชียงใหม่", "ต.สุเทพ",
                "อ.เมือง", "จ.เชียงใหม่", "50200"))

        var customer3 = customerRepository.save(Customer("ธนาธร", "thanathorn@life.com",
                UserStatus.PENDING))
        var address3 = addressRepository.save(Address("ซักที่บนโลก", "ต.สุขสันต์",
                "อ.ในเมือง", "จ.ขอนแก่น", "12457"))

        var shoppingCart1 = shoppingCartRepository.save(ShoppingCart(ShoppingCartStatus.SENT))
        var shoppingCart2 = shoppingCartRepository.save(ShoppingCart(ShoppingCartStatus.WAIT))

        var selected1 = selectedProductRepository.save(SelectedProduct(4))
        var selected2 = selectedProductRepository.save(SelectedProduct(1))
        var selected3 = selectedProductRepository.save(SelectedProduct(1))
        var selected4 = selectedProductRepository.save(SelectedProduct(1))
        var selected5 = selectedProductRepository.save(SelectedProduct(2))

        manul.products.add(product1)
        manu3.products.add(product2)
        manul.products.add(product3)
        manu2.products.add(product4)

        product1.manufacturer = manul
        product2.manufacturer = manu3
        product3.manufacturer = manul
        product4.manufacturer = manu2

        customer1.defaultAddress = address1
        customer2.defaultAddress = address2
        customer3.defaultAddress = address3

        selected1.product = product2
        selected2.product = product3
        selected3.product = product3
        selected4.product = product1
        selected5.product = product4



        shoppingCart1.customer = customer1
        shoppingCart1.selectedProduct.add(selected1)
        shoppingCart1.selectedProduct.add(selected2)

        shoppingCart2.customer = customer2
        shoppingCart2.selectedProduct.add(selected3)
        shoppingCart2.selectedProduct.add(selected4)
        shoppingCart2.selectedProduct.add(selected5)

        dataLoader.loadData()
        loadUsernameAndPassword()

    }

}